import { LANGUAGE_SELECTED } from '../actions/types';
import appOptions from '../appOptions';

const INITIAL_STATE = {
    language: appOptions.LANG.ENGLISH
};

const ProfileReducer = (state = INITIAL_STATE, action) => {
    console.log('++++ action ++++');
    console.log(action, state);
    switch (action.type) {
        case LANGUAGE_SELECTED:
            return { ...state, language: action.payload };
            
        default:
            return state;
    }
};

export default ProfileReducer;
