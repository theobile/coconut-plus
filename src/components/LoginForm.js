import React, { Component } from 'react';
import { View } from 'react-native';
import { Card, CardSection, Input, Button } from './common';

class LoginForm extends Component {
    render() {
        return (
            <View>
                <CardSection>
                    <Input
                        label='Name'
                        placeholder='Ashok Kumar'
                    />
                </CardSection>
                <CardSection>
                    <Input
                        label='Mobile Number'
                        placeholder='9845012345'
                    />
                </CardSection>
                 <CardSection>
                     <Input
                         label='Age'
                         placeholder='45'
                     />
                 </CardSection>
                 <CardSection>
                     <Button>
                         Login
                     </Button>
                 </CardSection>
            </View>
        );
    }
}

export default LoginForm;
