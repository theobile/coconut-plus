import React from 'react';
import { View } from 'react-native';

const Card = (props) => {
    return (
        <View style={styles}>
            {props.children}
        </View>
    );
};

const styles = {
    borderWidth: 1,
    borderColor: '#eee',
    borderBottomWidth: 0,
    borderRadius: 2,
    marginBottom: 5,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.2,
    shadowRadius: 2
};

export { Card };
