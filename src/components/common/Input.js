import React from 'react';
import { TextInput, View, Text } from 'react-native';

const Input = ({ label, placeholder, secureTextEntry, onChangeText, value }) => {
    const { containerStyle, labelStyle, inputStyle } = styles;
    return (
        <View style={containerStyle}>
            <Text style={labelStyle}>
                {label}
            </Text>
            <TextInput
                secureTextEntry={secureTextEntry}
                placeholder={placeholder}
                style={inputStyle}
                onChangeText={onChangeText}
                autoCorrect={false}
                autoCapitalize='none'
                value={value}
            />
        </View>
    );
};

const styles = {
    containerStyle: {
        height: 40,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    labelStyle: {
        fontSize: 18,
        flex: 1
    },
    inputStyle: {
        lineHeight: 23,
        // borderColor: '#eee',
        // borderWidth: 1,
        flex: 2,
        // borderRadius: 5,
        height: 35,
        paddingLeft: 5,
        paddingRight: 5
    }
};

export { Input };
