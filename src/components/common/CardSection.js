import React from 'react';
import { View } from 'react-native';

const CardSection = (props) => {
    return (
        <View style={styles.cardSectionStyles}>
            {props.children}
        </View>
    );
};

const styles = {
    cardSectionStyles: {
        // marginLeft: 5,
        // marginRight: 5,
        // borderWidth: 2,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        padding: 5,
        borderBottomWidth: 2,
        borderColor: '#eee',
        backgroundColor: '#fff'
    }
};

export { CardSection };
