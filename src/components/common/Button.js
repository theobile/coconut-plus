import React from 'react';
import { TouchableOpacity, Text } from 'react-native';

const Button = ({ children, onPress }) => {
    const { buttonStyle, textStyle } = styles;
    return (
        <TouchableOpacity style={buttonStyle} onPress={onPress}>
            <Text style={textStyle}>
                {children}
            </Text>
        </TouchableOpacity>
    );
};

const styles = {
    buttonStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 2,
        borderColor: '#007aff',
        borderRadius: 5,
        alignSelf: 'center',
        padding: 5
    },
    textStyle: {
        // flex: 1,
        fontSize: 18,
        fontWeight: '600',
        color: '#007aff',
        alignSelf: 'center'
    }
};

export { Button };
