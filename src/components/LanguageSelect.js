import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';

import { languageSelected } from '../actions';

import { Button } from './common';
import appOptions from '../appOptions';

class LanguageSelect extends Component {
    onLanguageSelect(lang) {
        this.props.languageSelected(lang);
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <Button onPress={() => this.onLanguageSelect(appOptions.LANG.ENGLISH)}>
                    English
                </Button>
                <Button onPress={() => this.onLanguageSelect(appOptions.LANG.KANNADA)}>
                    ಕನ್ನಡ
                </Button>
                <Button onPress={() => this.onLanguageSelect(appOptions.LANG.TELUGU)}>
                    తెలుగు
                </Button>
                <Button onPress={() => this.onLanguageSelect(appOptions.LANG.TAMIL)}>
                    தமிழ்
                </Button>
                <Button onPress={() => this.onLanguageSelect(appOptions.LANG.MALAYALAM)}>
                    മലയാളം
                </Button>
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        padding: 10,
        paddingTop: 80,
        paddingBottom: 80,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around'
    }
};

export default connect(null, { languageSelected })(LanguageSelect);
