const common = {
    ENGLISH: 'English',
    KANNADA: 'ಕನ್ನಡ',
    TAMIL: 'தமிழ்',
    TELUGU: 'తెలుగు',
    MALAYALAM: 'മലയാളം'
};

export default common;
