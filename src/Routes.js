import React from 'react';
import { Router, Stack, Scene } from 'react-native-router-flux';

import LanguageSelect from './components/LanguageSelect';
import LoginForm from './components/LoginForm';

const RouterComponent = () => {
    return (
        <Router>
            <Stack key="signup">
                <Scene key="languageSelect" component={LanguageSelect} title="Select Language" />
                <Scene key="login" component={LoginForm} title="Login" initial />
            </Stack>
         </Router>
    );
};

export default RouterComponent;
