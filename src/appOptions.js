const appOptions = {
    LANG: {
        ENGLISH: 'english',
        KANNADA: 'kannada',
        TELUGU: 'telugu',
        TAMIL: 'tamil',
        MALAYALAM: 'malayalam'
    }
};

export default appOptions;
