import { Actions } from 'react-native-router-flux';
import { LANGUAGE_SELECTED } from './types';

export const languageSelected = (text) => {
    return (dispatch) => {
        dispatch({
            type: LANGUAGE_SELECTED,
            payload: text
        });
        Actions.login();
    };
};
